from __future__ import unicode_literals
from django.db import models
from datetime import date, datetime
from django.utils import timezone
import re
import bcrypt

Name_Regex = re.compile(r'^[A-Za-z ]+$')

# Create your models here.
class userManager(models.Manager):
    def validate (self, postData):
        errors = []
        if len(postData['name']) < 2:
            errors.append("Name needs to be more than 1 letter")
        if not Name_Regex.match(postData['name']):
            errors.append("name can only be letters")
        if len(User.objects.filter(email = postData['email'])) > 0:
            errors.append("email already exists")
        if len(postData["email"])==0:
            errors.append("Please enter an email address")
        elif not re.search(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+.[a-zA-Z]+$', postData["email"]):
            errors.append("Please insert a valid email address")
        if postData['password'] != postData['confirm_password']:
            errors.append("Your passwords don't match")
        if len(errors) == 0:
            #create the user
            newuser = User.objects.create(name= postData['name'], email= postData['email'], password= bcrypt.hashpw(postData['password'].encode(), bcrypt.gensalt()))
            return (True, newuser)
        else:
            return (False, errors)

    def login(self, postData):
        errors = []
        if 'email' in postData and 'password' in postData:
            try:
                user = User.objects.get(email = postData['email'])#userManage acceses the database using .get (finds that one user's object)
            except User.DoesNotExist: #if the user doesnt exist from the .get(.get returns nothin, this 'except' prevents an error message)
                errors.append("Sorry, please try logging in again")
                return (False, errors)
        #password field/check
        pw_match = bcrypt.hashpw(postData['password'].encode(), user.password.encode())
        print 10*"3", user.password
        if pw_match == user.password:
            return (True, user)
        else:
            errors.append("Sorry please try again!")
            return (False, errors)

    def edit_profile(self, postData, user_id):
        update_time= self.filter(id = user_id).update(name = postData['edit_name'], address = postData['edit_address'], mobile_num = postData['edit_mobile_num'], home_phone = postData['edit_home_phone'])

        return (True, update_time)


    # def addDog(self, id, Dog_id):
    #     if len(User.objects.filter(Dogs=Dog_id, id=id))>0:
    #         return {'errors':'You already added this Dog'}
    #     else:
    #         adder = self.get(id=id)
    #         addDog= self.get(id = Dog_id)
    #         addDog.Dogs.add(adder)
    #         return {}
    #
    # def removeDog(self, id, Dog_id):
    #     if len(User.objects.filter(Dogs=Dog_id, id=id))==0:
    #         return {'errors':'You have already removed this Dog before'}
    #     else:
    #         removeuser= self.get(id=id)
    #         removeDog= self.get(id=Dog_id)
    #         removeuser.Dogs.remove(removeDog)
    #         print removeuser , removeDog
    #         return {}

class User(models.Model):
    name = models.CharField(max_length=45)
    password = models.CharField(max_length=100)
    email= models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    mobile_num = models.CharField(max_length=45, blank=True, null=True)
    home_phone = models.CharField(max_length=45, blank=True, null=True)

    objects = userManager()

    def __str__(self):
        return self.name



class appointManager(models.Manager):
    def appointval(self, postData, id):
        errors = []
        # print str(datetime.today()).split()[1]-> to see just the time in datetime
        print postData["time"]
        print datetime.now().strftime("%H:%M")
        if postData['date']:
            if not postData["date"] >= unicode(date.today()):
                errors.append("Date must be set in future!")
            if len(postData["date"]) < 1:
                errors.append("Date field can not be empty")
            print "got to appointment post Data:", postData['date']
        # if len(Appointment.objects.filter(date = postData['date'] ,time= postData['time'])) > 0:
        #     errors.append("Can Not create an appointment on past date and time")
        if len(postData['task'])<1:
            errors.append("Please write description")
        if len(errors)==0:
            makeappoint= Appointment.objects.create(user=User.objects.get(id=id), task= postData['task'],date= str(postData['date']),time= postData['time'],Options = postData['edit_Options'],breed =postData['breed'] )
            return(True, makeappoint)
        else:
            return(False, errors)

    def edit_appointment(self, postData, app_id):
        errors = []
        print errors
        # if postData['edit_date']:
        if not postData["edit_date"] >= unicode(date.today()):
            errors.append("Appointment date can't be in the past!")
            print "appoint date can't be past"
        if postData["edit_date"] == "" or len(postData["edit_tasks"]) < 1:
            errors.append("All fields must be filled out!")
            print "all fields must fill out pop out"
        if errors == []:
            update_time= self.filter(id = app_id).update(task = postData['edit_tasks'], Options = postData['edit_Options'], time = postData['edit_time'], date = postData['edit_date'],breed =postData['breed'] )

            return (True, update_time)
        else:
            return (False, errors)


class own_dog(models.Model):
    user =  models.ForeignKey(User, related_name="oneDog", blank=True, null=True)
    name = models.CharField(max_length=45,blank=True, null=True)
    breed = models.CharField(max_length=45,blank=True, null=True)
    age= models.CharField(max_length=45,blank=True, null=True)


class Appointment(models.Model):
    user= models.ForeignKey(User, related_name="onrecord", blank=True, null=True)
    task= models.CharField(max_length=255)
    Options= models.CharField(max_length=255)
    date= models.DateField(blank=True, null=True)
    time= models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    breed = models.CharField(max_length=255)
    objects= appointManager()

    def __str__(self):
        return str(self.user) +": "+str(self.Options) +" @ "+str(self.date) +" | "+str(self.time)
