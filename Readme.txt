Usage(python2.7):

cd Grooming_Appointments
pip install -r requirements.txt         # install the dependancies
python manage.py runserver              #start server at http://127.0.0.1:8000/

File description:
/appointments/settings.py          # settings for Django project
/apps/appointments_app/models.py   # database layer
/apps/appointments_app/routes.py   # routers
/apps/appointments_app/views.py    # view controls
/apps/appointments_app/templates   # render templates

Admin system account : tom
password: tom123456

TODO :  email notification
